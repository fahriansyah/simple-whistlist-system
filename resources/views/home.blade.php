@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
            <hr>
            @foreach($products as $product)
            <hr>
            <div class="card">
                <div class="card-header">{{$product->name}}</div>

                <div class="card-body">
                    {{$product->description}}
                </div>

                <div class="card-footer">
                  <a href="{{route('add.whistlist', $product->id)}}" class="btn btn-primary">Whistlist</a>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection
