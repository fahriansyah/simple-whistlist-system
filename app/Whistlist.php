<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Product;

class Whistlist extends Model
{
    //
    protected $table = "whistlists";

    public static function getProduct($id)
    {
      $product = Product::find($id);
      return $product;
    }
}
