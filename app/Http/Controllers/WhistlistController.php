<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Whistlist;
use Auth;

class WhistlistController extends Controller
{
    //add ke wistlist
    public function addToWhistlist($id)
    {
      $whistlist = new Whistlist;
      $whistlist->user_id = Auth::user()->id;
      $whistlist->product_id = $id;
      $whistlist->save();

      return back();
    }

    //melihat daftar whistlist
    public function listWhistlist($id)
    {
      $whistlist = Whistlist::where('user_id', $id)->first();
      return response()->json(['status' => 200, 'message' => 'OKE', 'data' => $whistlist->getProduct($whistlist->product_id)]);
    }
}
